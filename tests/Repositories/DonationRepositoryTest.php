<?php namespace Tests\Repositories;

use App\Models\Donation;
use App\Repositories\DonationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DonationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DonationRepository
     */
    protected $donationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->donationRepo = \App::make(DonationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_donation()
    {
        $donation = factory(Donation::class)->make()->toArray();

        $createdDonation = $this->donationRepo->create($donation);

        $createdDonation = $createdDonation->toArray();
        $this->assertArrayHasKey('id', $createdDonation);
        $this->assertNotNull($createdDonation['id'], 'Created Donation must have id specified');
        $this->assertNotNull(Donation::find($createdDonation['id']), 'Donation with given id must be in DB');
        $this->assertModelData($donation, $createdDonation);
    }

    /**
     * @test read
     */
    public function test_read_donation()
    {
        $donation = factory(Donation::class)->create();

        $dbDonation = $this->donationRepo->find($donation->id);

        $dbDonation = $dbDonation->toArray();
        $this->assertModelData($donation->toArray(), $dbDonation);
    }

    /**
     * @test update
     */
    public function test_update_donation()
    {
        $donation = factory(Donation::class)->create();
        $fakeDonation = factory(Donation::class)->make()->toArray();

        $updatedDonation = $this->donationRepo->update($fakeDonation, $donation->id);

        $this->assertModelData($fakeDonation, $updatedDonation->toArray());
        $dbDonation = $this->donationRepo->find($donation->id);
        $this->assertModelData($fakeDonation, $dbDonation->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_donation()
    {
        $donation = factory(Donation::class)->create();

        $resp = $this->donationRepo->delete($donation->id);

        $this->assertTrue($resp);
        $this->assertNull(Donation::find($donation->id), 'Donation should not exist in DB');
    }
}
