<?php namespace Tests\Repositories;

use App\Models\Donator;
use App\Repositories\DonatorRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DonatorRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DonatorRepository
     */
    protected $donatorRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->donatorRepo = \App::make(DonatorRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_donator()
    {
        $donator = factory(Donator::class)->make()->toArray();

        $createdDonator = $this->donatorRepo->create($donator);

        $createdDonator = $createdDonator->toArray();
        $this->assertArrayHasKey('id', $createdDonator);
        $this->assertNotNull($createdDonator['id'], 'Created Donator must have id specified');
        $this->assertNotNull(Donator::find($createdDonator['id']), 'Donator with given id must be in DB');
        $this->assertModelData($donator, $createdDonator);
    }

    /**
     * @test read
     */
    public function test_read_donator()
    {
        $donator = factory(Donator::class)->create();

        $dbDonator = $this->donatorRepo->find($donator->id);

        $dbDonator = $dbDonator->toArray();
        $this->assertModelData($donator->toArray(), $dbDonator);
    }

    /**
     * @test update
     */
    public function test_update_donator()
    {
        $donator = factory(Donator::class)->create();
        $fakeDonator = factory(Donator::class)->make()->toArray();

        $updatedDonator = $this->donatorRepo->update($fakeDonator, $donator->id);

        $this->assertModelData($fakeDonator, $updatedDonator->toArray());
        $dbDonator = $this->donatorRepo->find($donator->id);
        $this->assertModelData($fakeDonator, $dbDonator->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_donator()
    {
        $donator = factory(Donator::class)->create();

        $resp = $this->donatorRepo->delete($donator->id);

        $this->assertTrue($resp);
        $this->assertNull(Donator::find($donator->id), 'Donator should not exist in DB');
    }
}
