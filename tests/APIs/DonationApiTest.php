<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Donation;

class DonationApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_donation()
    {
        $donation = factory(Donation::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/donations', $donation
        );

        $this->assertApiResponse($donation);
    }

    /**
     * @test
     */
    public function test_read_donation()
    {
        $donation = factory(Donation::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/donations/'.$donation->id
        );

        $this->assertApiResponse($donation->toArray());
    }

    /**
     * @test
     */
    public function test_update_donation()
    {
        $donation = factory(Donation::class)->create();
        $editedDonation = factory(Donation::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/donations/'.$donation->id,
            $editedDonation
        );

        $this->assertApiResponse($editedDonation);
    }

    /**
     * @test
     */
    public function test_delete_donation()
    {
        $donation = factory(Donation::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/donations/'.$donation->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/donations/'.$donation->id
        );

        $this->response->assertStatus(404);
    }
}
