<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Donator;

class DonatorApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_donator()
    {
        $donator = factory(Donator::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/donators', $donator
        );

        $this->assertApiResponse($donator);
    }

    /**
     * @test
     */
    public function test_read_donator()
    {
        $donator = factory(Donator::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/donators/'.$donator->id
        );

        $this->assertApiResponse($donator->toArray());
    }

    /**
     * @test
     */
    public function test_update_donator()
    {
        $donator = factory(Donator::class)->create();
        $editedDonator = factory(Donator::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/donators/'.$donator->id,
            $editedDonator
        );

        $this->assertApiResponse($editedDonator);
    }

    /**
     * @test
     */
    public function test_delete_donator()
    {
        $donator = factory(Donator::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/donators/'.$donator->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/donators/'.$donator->id
        );

        $this->response->assertStatus(404);
    }
}
