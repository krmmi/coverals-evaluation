<?php
namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\GumballMachine;

class testGumballMachine extends TestCase
{
    public $gumballMachineInstance;

    public function setUp():void{
        $this->gumballMachineInstance = new GumballMachine();
    }
    public function testTurnWheel()
    {
        //Suppose we have 100 gumballs...
        $this->gumballMachineInstance->setGumballs(100);

        //... And we turn the wheel once...
        $this->gumballMachineInstance->turnWheel();

        //...we should now have 99 gumballs remaining in the machine right?
        $this->assertEquals(99, $this->gumballMachineInstance->getGumballs());
    }
}
